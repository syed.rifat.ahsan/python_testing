from unittest import mock
from functions_to_test import roll_dice, win_loose, get_ip
import pytest


@pytest.mark.parametrize("inp,expected", [(3, True), (4, False), (5, False), (6, False)])
@mock.patch("functions_to_test.roll_dice")
def test_win_loose(mock_roll_dice, inp, expected):
    mock_roll_dice.return_value = inp
    assert win_loose() == expected


@mock.patch("functions_to_test.requests.get")
def test_get_ip(mock_request_get_value):
    mock_request_get_value.return_value= mock.Mock(
        name="mock response", **{"status_code": 200, "json.return_value": {"origin": "0.0.0.0"}})
    assert get_ip() == "0.0.0.0"

@mock.patch("functions_to_test.win_loose")
def test_win_loose_1(mock_win_loose):
    mock_win_loose.return_value = True
    assert mock_win_loose() == True
