import requests
from exception_decorator import handle_exception
import random


def roll_dice():
    return random.randint(1, 6)


def win_loose():
    if roll_dice() == 3:
        return True
    return False


def get_ip():
    response = requests.get("http://httpbin.org/ip")
    
    if response.status_code == 200:
        return response.json()['origin']
